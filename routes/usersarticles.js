var router = require('express').Router();
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('blog_db.db');
var date_time = require('node-datetime');


// Add A New Article
router.get("/add", function (req, res) {
	res.render("add_article");
});

// When the user POSTs to "/articles", add a new article to the database, then redirect back to articles display -- future: redirect to profile page with their article history.
router.post("/add", function (req, res) {
	// setting up date_time
	var dt = date_time.create();
	var pub_date = dt.format('d f Y');
	// insert new article into articles table
	var userId = req.user.id;
	var articleTitle = req.body.title;
	var articleContent = req.body.content;


	db.run("insert into articles (title, content, authorID, pub_date ) values (?, ?, ? , ?)", [articleTitle, articleContent, userId, pub_date], function (err) {
		console.log(err);
		console.log(this);
		console.log("New article added with id = " + this.lastID);
		console.log(this.changes + " row(s) affected.");

		res.redirect("/userhome");
	});
});

// Load Edit Article Form - grab the article from the db and pass it to the edit form
router.get("/edit/:id", function (req, res) {
	var article = req.params.id;

	// get the article from the articles table, pass it along to the view and render the   article details template
	db.each("select id, title, content from articles where id = ?", [article], function (err, row) {

		var article = row;
		var data = {
			title: article.title,
			content: article.content,
			id: article.id,
			username: req.user.username
		};

		res.render("edit_article", data);
	});
});

// When the user POSTs to "/articles/edit/:id query the db and update the article that matches params.id"
router.post("/edit/:id", function (req, res) {

	var article = req.params.id;

	// grab edited article form fields
	var editedTitle = req.body.title;
	var editedContent = req.body.content;

	// query the database to update the article with id = req.params.id
	db.run("update articles set title = ?, content = ? where id = ?", [editedTitle, editedContent, article], function (err) {

		console.log(this);
		console.log("New article added with id = " + this.lastID);
		console.log(this.changes + " row(s) affected.");

		res.redirect("/userhome");
	});
});

// delete article that matches params.id in the articles table
router.get("/delete/:id", function (req, res) {

	var article = req.params.id;
	console.log(article);
	db.run("delete from articles where id = ?", [article], function (err) {
		if (err) {
			console.log(err);
		} else {
			console.log(this);
			console.log(this.changes + " row(s) affected.");
		}
		res.redirect("/userhome");
	});
});

// When the user POSTs to "/articles/id/comment", add comment to the database, then redirect back to article page they came from, passing in the comment data
router.post("/comment/:id", function (req, res) {

	console.log("you've posted a comment on article " + req.params.id);
	var comment = req.body.comment;
	var article = req.params.id;

	db.run("insert into comments (comment, articleId) values (?, ?)", [comment, article], function (err) {

		if (err) {
			console.log(err);
		}

		console.log(this);
		console.log("New comment added with id = " + this.lastID);
		console.log(this.changes + " row(s) affected.");

		var data = {
			comment: comment
		}
		res.render("/userhome", data);
	});
});


// Display a single article
router.get("/:id", function (req, res) {
	var article = req.params.id;


	// get the article from the articles table, pass it along to the view and render the article details template
	db.each("select id, title, content, authorId from articles where id = ?", [article], function (err, row) {

		var article = row;

		var data = {
			title: article.title,
			content: article.content,
			id: article.id,
			authorId: article.authorId,
			username: req.user.username
		};

		res.render("usersarticles", data);
	});
});

module.exports = router;