var router = require('express').Router();
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('blog_db.db');


router.get("/edit/:username", function(req, res){
	var username = req.params.username;
	
	// get the article from the articles table, pass it along to the view and render the article details template
	db.each("select id, fname, lname, username, password, dob, country, description from users where username = ?", [username], function(err, row){
		
		var user = row;
		
		var data = {
			id: user.id,
            fname: user.fname,
            lname: user.lname,
            username: user.username,
			password: user.password,
			dob: user.dob,
			country: user.country,
			description: user.description
		
		};

		console.log(data);
		
		res.render("editdetails", data);
	});
});

router.post("/edit/:username", function(req, res){
	
	var username = req.params.username;
	
	// grab edited article form fields
	var fname = req.body.fname;
	var lname = req.body.lname;
	var editedusername= req.body.username;
	var password=  req.body.password;
	var dob= req.body.dob;
	var country=  req.body.country;
	var description= req.body.description;
	
	// query the database to update the article with id = req.params.id
	db.run("update users set fname = ?, lname = ?, username = ?, password =?, dob = ?, country =?, description= ?  where username = ?", [fname, lname,editedusername,password,dob, country , description, username], function(err){
  
		console.log(this);
        console.log("New article added with id = " + this.lastID);
        console.log(this.changes + " row(s) affected.");
	
		// res.redirect("/userhome");
	});	

		var data ={
			username: editedusername,
			fname: fname,
            lname: lname,
            password: password,
			dob: dob,
			description: description,
			country: country
			
		}

	res.render("editdetails", data );
	
});


router.get("/edit/:username", function(req, res){
	var username = req.params.username;
	

		
		res.render("deleteaccount", data);
	});




router.post("/delete/:username", function(req, res){
	
	var username = req.params.username;
	
	// grab edited article form fields
	var fname = req.body.fname;
	var lname = req.body.lname;
	var editedusername= req.body.username;
	var password=  req.body.password;
	var dob= req.body.dob;
	var country=  req.body.country;
	var description= req.body.description;
	
	// query the database to update the article with id = req.params.id
	db.run("update users set fname = ?, lname = ?, username = ?, password =?, dob = ?, country =?, description= ?  where username = ?", [fname, lname,editedusername,password,dob, country , description, username], function(err){
  
		// console.log(this);
        // console.log("New article added with id = " + this.lastID);
        // console.log(this.changes + " row(s) affected.");
	
		// res.redirect("/userhome");
	});	

		var data ={
			username: editedusername,
			fname: fname,
            lname: lname,
            password: password,
			dob: dob,
			description: description,
			country: country
			
		}

	res.render("editdetails", data );
	
});




module.exports = router;