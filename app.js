// Setup code
// --------------------------------------------------------------------------
var express = require('express');
var articlesRoute = require('./routes/articles');
var usersarticlesRoute = require('./routes/usersarticles');
var usersRoute = require('./routes/users');
var formidable = require('formidable');
var jimp = require('jimp');
var fs = require('fs');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({
	defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

// Specify that the app should use node-datetime
var dateTime = require('node-datetime');

// Specify that the app should use body parser (for reading submitted form data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
	extended: true
})); // Allows us to read forms submitted with POST requests

// serve public files
app.use(express.static(__dirname + "/public"));

// Allow us to use SQLite3 from node.js
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('blog_db.db');

// Passport setup code
// --------------------------------------------------------------------------

// Specify that the app should use "passport" for authentication, and
// that the authentication type should be "local".
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

// Specify that the app should use express-session to create in-memory sessions
var session = require('express-session');
app.use(session({
	resave: false,
	saveUninitialized: false,
	secret: "compsci719"
}));



// Route handlers
// --------------------------------------------------------------------------
// Home Route - Display all articles
app.get(["/", "/home"], function (req, res) {
	db.all("select a.id, a.title, substr(a.content, 1, 100) || '...' as 'content', a.pub_date, u.fname, u.lname, u.profile_pic from articles a, users u where a.authorId == u.id", function (err, rows) {
		if (err) {
			console.log(err);
		}

		console.log(rows);
		var data = {
			articles: rows
		}
		res.render("home", data);
	});
});

// Route Files
app.use("/articles", articlesRoute);

// Register Form
app.get("/register", function (req, res) {
	res.render("register");
});

// Register process
app.post("/register", function (req, res) {

	//create form object

	var form = new formidable.IncomingForm();

	// saving the fullsize images to the full_size folder
	// if()

	form.on('fileBegin', function (name, file) {
		if (file.name) {
			console.log(file);
			file.path = __dirname + '/public/picture_uploads/full_size/' + file.name;

		}
	});
	// parsing the form

	form.parse(req, function (err, fields, files) {

		console.log(err);
		//console.log(files);
		// get the uploaded file

		if (files.fileUpload && files.fileUpload.name) {
			jimp.read(__dirname + '/public/picture_uploads/full_size/' + files.fileUpload.name, function (err, image) {
				console.log(err);
				//After reading the image, create it's thumbnail by scaling it to fit to 120 x 120 box
				image
					.scaleToFit(120, 120)
					.write(__dirname + '/public/picture_uploads/thumbnail/' + files.fileUpload.name, function (err) {
						if (err) {
							console.error('error saving file' + 'thumbnail');
						} else {
							console.log('Success!');
						}
					});
			});
		}

		var fname = fields.fname;
		var lname = fields.lname;
		var username = fields.username;
		var password = fields.password;
		var dob = fields.dob;
		var country = fields.country;
		var des = fields.description;
		var profile_pic;

		if (fields.profile_pic) {
			profile_pic = fields.profile_pic;
		} else {
			profile_pic = "/picture_uploads/thumbnail/" + files.fileUpload.name;
		}
		console.log(profile_pic);

		db.run("INSERT into USERS (fname, lname, username, password, dob, country, description, profile_pic) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [fname, lname, username, password, dob, country, des, profile_pic], function (err) {

			console.log(err);

			console.log(this);
			console.log("New user added with id = " + this.lastID);
			console.log(this.changes + " row(s) affected.");
			res.redirect('login');
		});
	});


});


var users = [];


db.all("SELECT id, username , password  FROM users ", function (err, rows) {

	users = rows;

	return users;

	// console.log(users);

});

function findUser(username) {
	/*return users.find(function (user) {
	    return user.username == username;
	});*/
	console.log(username);

	for (var i = 0; i < users.length; i++) {
		if (users[i].username == username) {
			return users[i];
		}
	}
	return undefined;
}

// Set up local authentication
var localStrategy = new LocalStrategy(
	function (username, password, done) {

		// Get the user from the "database"
		user = findUser(username);

		// If the user doesn't exist...
		if (!user) {
			return done(null, false, {
				message: 'Invalid user'
			});
		};

		// If the user's password doesn't match the typed password...
		if (user.password !== password) {
			return done(null, false, {
				message: 'Invalid password'
			});
		};

		// If we get here, everything's OK.
		done(null, user);
	}
);

// This method will be called when we need to save the currently
// authenticated user's username to the session.
passport.serializeUser(function (user, done) {
	done(null, user.username);
});

// This method will be called when we need to get all the data relating
// to the user with the given username.
passport.deserializeUser(function (username, done) {
	user = findUser(username);
	done(null, user);
});

// Set up Passport to use the given local authentication strategy
// we've defined above.
passport.use('local', localStrategy);

// Start up Passport, and tell it to use sessions to store necessary data.
app.use(passport.initialize());
app.use(passport.session());

// A helper function, which will check if there's a user that's logged in.
// If there is an authenticated user, then "next()" will be called,
// which will forward the request onto the actual page to be displayed.
// If there is no authenticated user, then they'll be redirected to
// the homepage.
// See below for an example of how to use this function.
function isLoggedIn(req, res, next) {
	// if user is authenticated, carry on 
	if (req.isAuthenticated()) {
		return next();
	}

	// redirect them to the home page
	res.redirect("/login");
}

app.get("/login", function (req, res) {
	if (req.isAuthenticated()) {
		res.redirect("/userhome");
	} else {
		// This data will be used to display helpful error / info panels on the homepage.
		var data = {
			layout: "no-navbar", // The login page doesn't use the navbar / shopping cart.
			loginFail: req.query.loginFail // Will be set when the user is redirected here upon failing to login.
		}
		res.render("login", data);
	}
});

app.post('/login', passport.authenticate('local', {
	successRedirect: '/userhome',
	failureRedirect: '/login?loginFail=true'
}));

app.get("/userhome", isLoggedIn, function (req, res) {
	db.all("select a.id, a.title, substr(a.content, 1, 100) || '...' as 'content', a.pub_date, u.fname, u.lname, u.profile_pic from articles a, users u where a.authorId == u.id", function (err, rows) {
		if (err) {
			console.log(err);
		}

		var data = {
			thisPage: "/userhome",
			username: req.user.username,
			id: req.user.id,
			articles: rows
		}
		res.render("userhome", data);
	});
});


// app.get("/userhome/editdetails", function(req,res){

// 	var userId = req.params.id;

// 	// get the users from the users table, pass it along to the view and render the  editdetails template to edit users table
// 	db.each("select id, fname, lname, usernname, password, dob, country, description from users where id = ?", [userID], function(err, row){

// 		var user = row;

// 		var data = {
// 			id: user.id,
//             fname: user.fname,
//             lname: user.lname,
//             username: user.username,
// 			password: user.password,
// 			dob: user.dob,
// 			country: user.country,
// 			description: user.description	

// 		};

// 		res.render("editdetails", data);
// 	});

// 	// res.render("/users");

// });



// If the user navigates to "/logout", log them out and then redirect them
// to the animals page.
app.get("/logout", function (req, res) {
	req.logout();
	res.redirect("/home?loggedOut=true");
});


// Route Files
app.use("/userhome/usersarticles", usersarticlesRoute);
app.use("/userhome/users", usersRoute);




// Start the server running.
app.listen(app.get('port'), function () {
	console.log('Express started on http://localhost:' + app.get('port'));
});